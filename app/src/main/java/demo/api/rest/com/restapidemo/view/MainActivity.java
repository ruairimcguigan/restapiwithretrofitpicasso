package demo.api.rest.com.restapidemo.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import demo.api.rest.com.restapidemo.R;
import demo.api.rest.com.restapidemo.base.BaseActivity;
import demo.api.rest.com.restapidemo.controller.Controller;
import demo.api.rest.com.restapidemo.model.adapter.ListAdapter;
import demo.api.rest.com.restapidemo.model.pojo.Flower;

public class MainActivity extends BaseActivity implements View.OnClickListener, Controller.FlowerCallbackListener{

    Toolbar mToolbar;
    SwipeRefreshLayout mSwipeRefreshLayout;
    FloatingActionButton mFloatingActionButton;
    RecyclerView mRecyclerView;
    Controller mController;

    ArrayList<Flower> mFlowerArrayList = new ArrayList<>();
    ListAdapter mListAdapter;

    @Override
    protected void onStart() {
        super.onStart();
        mController = new Controller(MainActivity.this);
        mController.startFetching();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initViews();

    }

    private void initViews() {

        mFloatingActionButton = ( FloatingActionButton ) findViewById(R.id.fab);
        mFloatingActionButton.setOnClickListener(this);

        mRecyclerView = ( RecyclerView ) findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        mListAdapter = new ListAdapter(mFlowerArrayList);
        mRecyclerView.setAdapter(mListAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout ) findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mController.startFetching();
            }
        });
    }

    private void initToolbar() {
        mToolbar= ( Toolbar ) findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(mToolbar);

        if(mToolbar!=null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void addFlowers() {
        for(int i = 0; i  < 1000; i++) {

            Flower flower = new Flower.Builder()
                    .setName("Filippo")
                    .setPrice(15.2)
                    .setPhoto("AJHAJKS")
                    .build();

            mListAdapter.addFlower(flower);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if ( id == R.id.action_settings ) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onFetchStart() {

    }

    @Override
    public void onFetchProgress(Flower flower) {
        mListAdapter.addFlower(flower);
    }

    @Override
    public void onFetchProgress(List< Flower > flowerList) {

    }

    @Override
    public void onFetchComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onFetchFailed() {

    }
}
