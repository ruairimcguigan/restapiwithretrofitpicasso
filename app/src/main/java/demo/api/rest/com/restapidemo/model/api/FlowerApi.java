package demo.api.rest.com.restapidemo.model.api;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by rmcguigan on 24/11/15.
 */
public interface FlowerApi {

    /**
     * Asynchronous
     */

    //Want the JSON in a string format
    @GET("/feeds/flowers.json")
    void getFlowers(Callback<String> flowers);

}
