package demo.api.rest.com.restapidemo.model.pojo;

/**
 * Created by rmcguigan on 23/11/15.
 */
public class Flower {

    public String mCategory,mInstructions, mPhoto, mName;
    public double mPrice;
    public int mProductID;

    private Flower(Builder builder){
        mCategory = builder.mCategory;
        mInstructions = builder.mInstructions;
        mPhoto = builder.mPhoto;
        mName = builder.mName;
        mPrice = builder.mPrice;
        mProductID = builder.mProductID;

    }

    public static class Builder{

        String mCategory,mInstructions, mPhoto, mName;
        double mPrice;
        int mProductID;

        public Builder setCategory(String category){
            mCategory = category;
            return Builder.this;
        }

        public Builder setInstructions(String instructions){
            mInstructions = instructions;
            return Builder.this;
        }

        public Builder setPhoto(String photo){
            mPhoto = photo;
            return Builder.this;
        }

        public Builder setName(String name){
            mName = name;
            return Builder.this;
        }

        public Builder setPrice(Double price){
            mPrice = price;
            return Builder.this;
        }

        public Builder setProductId( int id){
            mProductID = id;
            return Builder.this;
        }

        public Flower build(){
            return new Flower(Builder.this);
        }

    }

}
