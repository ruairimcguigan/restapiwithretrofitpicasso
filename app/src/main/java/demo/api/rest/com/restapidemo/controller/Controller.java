package demo.api.rest.com.restapidemo.controller;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import demo.api.rest.com.restapidemo.model.api.RestApiManager;
import demo.api.rest.com.restapidemo.model.pojo.Flower;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rmcguigan on 24/11/15.
 */
public class Controller {

    private static final String TAG = Controller.class.getSimpleName();
    private FlowerCallbackListener mFlowerCallbackListener;
    protected RestApiManager mApiManager;

    public Controller(FlowerCallbackListener listener){
        mFlowerCallbackListener = listener;
        mApiManager = new RestApiManager();
    }

    /**
     * Begin fetching the JSON feed
     */
    public void startFetching() {
        mApiManager.getFlowerApi().getFlowers(new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                Log.d(TAG, "JSON :: " + s);

                try {
                    JSONArray array = new JSONArray(s);

                    for(int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);

                        Flower flower = new Flower.Builder()
                                .setCategory(object.getString("category"))
                                .setPrice(object.getDouble("price"))
                                .setInstructions(object.getString("instructions"))
                                .setPhoto(object.getString("photo"))
                                .setName(object.getString("name"))
                                .setProductId(object.getInt("productId"))
                                .build();

                        mFlowerCallbackListener.onFetchProgress(flower);

                    }

                } catch (JSONException e) {
                    mFlowerCallbackListener.onFetchFailed();
                }

                mFlowerCallbackListener.onFetchComplete();
            }


            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "ERROR :: " + error.getMessage());
                mFlowerCallbackListener.onFetchComplete();
            }
        });
    }

    public interface FlowerCallbackListener{
        void onFetchStart();
        void onFetchProgress(Flower flower);
        void onFetchProgress(List<Flower> flowerList);
        void onFetchComplete();
        void onFetchFailed();
    }
}
