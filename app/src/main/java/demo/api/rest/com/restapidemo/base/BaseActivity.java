package demo.api.rest.com.restapidemo.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class BaseActivity extends AppCompatActivity {

    public static final String BASE_URL ="http://services.hanselandpetal.com";
    public static final String FLOWERS_URL = BASE_URL + "/feeds/flowers.json";
    public static final String PHOTO_URL = BASE_URL + "/photos/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void goToActivity(Class activityClassReference){

        Intent intent = new Intent(this, activityClassReference);
        startActivity(intent);
    }

    private void makeSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

}


