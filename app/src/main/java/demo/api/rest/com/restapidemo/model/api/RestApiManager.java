package demo.api.rest.com.restapidemo.model.api;

import com.google.gson.GsonBuilder;

import demo.api.rest.com.restapidemo.base.BaseActivity;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by rmcguigan on 24/11/15.
 */
public class RestApiManager {

    FlowerApi mFlowerApi;

    public FlowerApi getFlowerApi(){

        if ( mFlowerApi == null){

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(String.class, new StringDeserializer());

            mFlowerApi = new RestAdapter.Builder()
                    .setEndpoint(BaseActivity.BASE_URL)
                    .setConverter(new GsonConverter(gsonBuilder.create()))
                    .build()
                    .create(FlowerApi.class);
        }
        return mFlowerApi;
    }
}
