package demo.api.rest.com.restapidemo.model.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import demo.api.rest.com.restapidemo.R;
import demo.api.rest.com.restapidemo.base.BaseActivity;
import demo.api.rest.com.restapidemo.model.pojo.Flower;

/**
 * Created by rmcguigan on 23/11/15.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder > {

    public static String TAG = ListAdapter.class.getSimpleName();

    List<Flower> mFlowerList;

    public ListAdapter(List<Flower> flowers){
        mFlowerList = flowers;
    }

    public void addFlower(Flower flower){
        mFlowerList.add(flower);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
        return new ViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Flower currentFlower = mFlowerList.get(position);
        holder.mName.setText(currentFlower.mName);
        holder.mCategory.setText(currentFlower.mCategory);
        holder.mPrice.setText(Double.toString(currentFlower.mPrice));
        holder.mInstructions.setText(currentFlower.mInstructions);

        Picasso.with(holder.itemView.getContext()).load(BaseActivity.PHOTO_URL
                + currentFlower.mPhoto).into(holder.mImage);


    }

    @Override
    public int getItemCount() {
        return mFlowerList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mName, mCategory, mPrice, mInstructions;
        public ImageView mImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.flowerName);
            mCategory = (TextView) itemView.findViewById(R.id.flowerCategory);
            mPrice = (TextView) itemView.findViewById(R.id.flowerPrice);
            mInstructions = (TextView) itemView.findViewById(R.id.flowerInstruction);
            mImage = (ImageView) itemView.findViewById(R.id.flowerImage);
        }
    }
}
